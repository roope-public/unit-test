# Project 'unit-test'
## Overview
This project showcases the basics of unit testing in software testing
with the 'Mocha' testing framework and 'Chai', a BDD/TDD assertion library.

The repository has several folders and files of which the most relevant are the `'src/main.js'`, `'src/mylib.js'`, `'test'`-folder, `'package.json'` and the `'public'`-folder.

## main.js
This is the program itself. It is almost empty at this time. The only thing in it, is the importing of the `'mylib.js'` library so it can be used in the main program.

## mylib.js
Here we define the four basic mathematical operations: Addition (`add`), Subtraction (`subtract`), Multiplication (`multiply`) and Division (`divide`).

They are formatted slightly differently for educational purposes.

The division function contains an if-else -statement, to take into accordion division by zero. It throws an error if the divisor is zero.

## test-folder and its contents
The tests for each arithmetic function are in separate files. This made things easier to maintain and edit.

The tests for decimal numbers in `'add'` and `'subtract'` fail because of JavaScript's lack of accuracy with decimal numbers.

## package.json
This is mostly auto-generated. The `"test": "mocha"` replaced the original content so we can use the terminal command `'npm run test'` to run our tests. There's also `'"test:browser": "live-server --port=9000 --mount=/:public"'` for running the tests in a web interface with the command `'npm run test:browser'` which starts `live-server` and opens Firefox.

## public-folder and its contents
Here are files for running the tests and showing their results in a browser. The `'mylib.js'` is essentially the same as the one under `'src'`, but the functions are defined "straight up" instead of existing inside an object. This was done to test different ways of making things work.

There are test files only for the addition and subtraction function.

`'index.html'` contains the setup for making the web interface work. The contents are almost entirely copy-pasted from [here](https://blog.logrocket.com/testing-node-js-mocha-chai/).