
/** Basic arithmetic operations */
const mylib = {
    /** Multiline arrow functions: */
    add: (a, b) => {
        const sum = Number(a) + Number(b);
        return sum;
    },
    subtract: (a, b) => {
        return a - b;
    },
    /** Regular function: */
    divide: function(dividend, divisor) {
        if(divisor != 0){
            return dividend / divisor
        } else{
            throw new TypeError("Divisor can't be zero.")
        }
    },
    /** Singleline arrow function: */
    multiply: (a, b) =>  a * b
};

module.exports = mylib;