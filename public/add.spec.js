describe("Tests for 'add'-function", function(){
    before(function() {
        // Initialization:
        // Create objects etc...
        console.log("Initializing tests for 'add'.");
    });

    context("3 plus -5", function(){
        it("should be less than '0'", function(){
            expect(add(3,-5)).to.be.below(0);
        });
    });
    context("for whole numbers", function(){
        it("should return sum", function() {
            expect(add(1, 2)).equal(3, 
                "1 + 2 is not 3, for some reason?");
            expect(add(-4, 3)).equal(-1, 
                "Addition of '-4', '3' not working");
        });
    });

    after(() => {
        // Cleanup:
        // E.g: Shut down Express server
        console.log("Testing for 'add' completed.")
    });
});