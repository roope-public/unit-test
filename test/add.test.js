const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Tests for 'add' function", function() {
    before(function() {
        // Initialization:
        // Create objects etc...
        console.log("Initializing tests for 'add'.");
    });
    
    context("for whole numbers", function(){
        it("should return sum", function() {
            expect(mylib.add(1, 2)).equal(3, 
                "1 + 2 is not 3, for some reason?");
            expect(mylib.add(-4, 3)).equal(-1, 
                "Addition of '-4', '3' not working");
        });
    })

    context("for decimal numbers", function(){
        it("should return sum", function(){
            expect(mylib.add(1.023, -2.00058)).equal(-0.97758, 
                "Addition of '1.023', '-2.00058' not working (known js accuracy issue...)");
        });
    });

    after(() => {
        // Cleanup:
        // E.g: Shut down Express server
        console.log("Testing for 'add' completed.")
    });
});