const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Tests for 'subtract' function", function() {
    before(function() {
        // Initialization:
        // Create objects etc...
        console.log("Initializing tests for 'subtract'.");
    });
    
    context("for whole numbers", function(){
        it("should return difference", function() {
            expect(mylib.subtract(-3, -5)).equal(2, 
                "Subtraction of '-3', '-5' not working");
            expect(mylib.subtract(56, -5600)).equal(5656, 
                "Subtraction of '56', '-5600' not working");
        });
    })

    context("for decimal numbers", function(){
        it("should return difference", function(){
            expect(mylib.subtract(1.023, 2.58)).equal(-1.557, 
                "Subtraction of '1.023', '2.58' not working (known js accuracy issue...)");
        });
    });

    after(() => {
        // Cleanup:
        // E.g: Shut down Express server
        console.log("Testing for 'subtract' completed.")
    });
});