const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Tests for 'multiply' function", function() {
    before(function() {
        // Initialization:
        // Create objects etc...
        console.log("Initializing tests for 'multiply'.");
    });
    
    context("for whole numbers", function(){
        it("should return product", function() {
            expect(mylib.multiply(-3, -5)).equal(15, 
                "Multiplication of '-3', '-5' not working");
        });
    })

    context("for decimal numbers", function(){
        it("should return product", function(){
            expect(mylib.multiply(6, 0.01)).equal(0.06, 
                "Multiplication of '6', '0.01' not working");
        });
    });

    after(() => {
        // Cleanup:
        // E.g: Shut down Express server
        console.log("Testing fo 'multiply' completed.")
    });
});