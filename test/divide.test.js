const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Tests for 'divide' function", function() {
    before(function() {
        // Initialization:
        // Create objects etc...
        console.log("Initializing tests for 'divide'.");
    });
    
    context("for whole numbers", function(){
        it("should return quotient", function() {
            expect(mylib.divide(-3, -5)).equal(0.6, 
                "Division of '-3', '-5' not working");
        });
    })

    context("for decimal numbers", function(){
        it("should return quotient", function(){
            expect(mylib.divide(6, 0.01)).equal(600, 
                "Division of '6', '0.01' not working");
        });
    });
    
    context("for division by zero", function(){
        it("should throw 'TypeError'", function() {
            expect(function(){mylib.divide(5, 0)}).to.throw(TypeError,"Divisor can't be zero.");
        });
    });

    after(() => {
        // Cleanup:
        // E.g: Shut down Express server
        console.log("Testing for 'divide' completed.")
    });
});